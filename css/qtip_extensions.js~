var qTipExtensions = {
  notificationLifeTime: 5000,
  initialTitle: '',
  showingTitle: true,
  titleInterval: null,
  queue: [],
};

// Taken from http://craigsworks.com/projects/qtip2/demos/ and modified.
(function($) {
  qTipExtensions.initialTitle = $('title').html();

  $.fn.qtip.defaults.events.render = function(event, api) {
    // Move the close button into the highest possible level inside the tooltip and remove the standard X
    var closeButton = $(api.elements.tooltip)
            .find('span.ui-icon-close');

    closeButton.html('')
            .parent()
            .prependTo(api.elements.tooltip);

    // Replace the titlebar with an h3
    var titlebar = $(api.elements.tooltip).find('div.ui-tooltip-titlebar');
    var content = titlebar.html();
    titlebar.replaceWith('<h3 class="ui-tooltip-title">' + content + '</h3>');

    // Drupal.behaviors.modalDialog.attach($(api.elements.tooltip));
  };

  qTipExtensions.updateTitle = function() {
    var amountOfMessages = $('.qtip.qtip-extensions-notification').length;

    if (amountOfMessages === 0) {
      clearInterval(qTipExtensions.titleInterval);
      $('title').html(qTipExtensions.initialTitle);
      qTipExtensions.showingTitle = true;
    }

    if (amountOfMessages > 0) {
      if (qTipExtensions.showingTitle) {
        $('title').html(Drupal.formatPlural(
                amountOfMessages,
                '@count new message',
                '@count new messages'
                ));
        qTipExtensions.showingTitle = false;
      } else {
        $('title').html(qTipExtensions.initialTitle);
        qTipExtensions.showingTitle = true;
      }
    }

    if (qTipExtensions.titleInterval === null) {
      qTipExtensions.titleInterval = setInterval(function() {
        qTipExtensions.updateTitle();
      }, 2000);
    }
  };

  qTipExtensions.queue_notification = function(message) {
    qTipExtensions.queue.push(message);
  }

  /**
   * Shows a notification in the upper right corner of the page.
   * @param message Message that you want to show in the notification.
   * @param title Title of the notification, default: Notification
   * @param persistent Wether the notification shall be persistent or not, default: false
   * @param qTipSettings Additional settings to add to the tooltip. Consult qTip documentation for further info.
   */
  qTipExtensions.notification = function(message, title, persistent, qTipSettings) {
    if ($('.qtip-extensions-notification').length > 5) {
      qTipExtensions.notification($('.qtip-extensions-notification').length);
      qTipExtensions.queue_notification({message: message, title: title, persistent: persistent, qTipSettings: qTipSettings});
      return;
    }

    if (message === null || message === false || message === '')
      return;

    if (title === null || title === false || title === '')
      title = Drupal.t('Notification');

    if (persistent === null || persistent === '')
      persistent = false;

    var target = $('.qtip.qtip-extensions-notification:visible:last');

    var configuration = {
      content: {
        text: message,
        title: {
          text: title,
          button: true
        }
      },
      position: {
        my: 'top right',
        at: (target.length ? 'bottom' : 'top') + ' right',
        target: target.length ? target : $(window),
        adjust: {
          y: 10,
          x: -5
        },
        effect: function(api, newPos) {
          $(this).animate(newPos, {
            duration: 200,
            queue: false
          });
          api.cache.finalPos = newPos;
        }
      },
      show: {
        event: false,
        ready: true,
        effect: function() {
          $(this).stop(0, 1).fadeIn(400);
        },
        delay: 0,
        persistent: persistent
      },
      hide: {
        event: false,
        effect: function(api) {
          $(this).stop(0, 1).fadeOut(400).queue(function() {
            api.destroy();
            qTipExtensions.update_notifications();
          });
        }
      },
      style: {
        classes: Drupal.settings.qTipExtensions['tour additional classes'] + ' qtip-extensions-notification',
        tip: false
      },
      events: {
        render: function(event, api) {
          $.fn.qtip.defaults.events.render(event, api);
          timer.call(api.elements.tooltip, event);
        },
        visible: function(event, api) {
          // Reposition the notification to enlarge the distance to the right window border
          var tooltip = api.elements.tooltip;

          var tooltipOffset = $(tooltip).offset();
          var tooltipWidth = $(tooltip).outerWidth();
          var windowWidth = $(window).width();

          if (tooltipOffset.left + tooltipWidth === windowWidth) {
            $(tooltip).offset({
              left: tooltipOffset.left - 15,
              top: tooltipOffset.top
            });
          }
        }
      }
    };

    if (qTipSettings !== null && typeof qTipSettings === 'object')
      $.extend(true, configuration, configuration, qTipSettings);
    $(window).qtip(configuration).removeData('qtip');

    qTipExtensions.updateTitle();
  };

  /**
   * Hides expired notifications. Doesn't have to be called manually.
   */
  qTipExtensions.update_notifications = function() {
    var
            lastX = 0,
            pos;

    $('.qtip.qtip-extensions-notification').each(function(i) {
      var api = $(this).data('qtip');

      api.options.position.target = !i
              ? $(window)
              : [$(this).offset().left + $(this).outerWidth(), lastX];

      api.set('position.at', 'top right');

      if (i === 0)
        api.set('position.adjust.x', -20);

      if (!i)
        pos = api.cache.finalPos;

      lastX += pos.top + $(this).outerHeight() + 5;
    });

    qTipExtensions.updateTitle();
  };

  function timer(event) {
    var api = $(this).data('qtip');

    if (api.get('show.persistent') === true) {
      return;
    }

    clearTimeout(api.timer);
    if (event.type !== 'mouseover') {
      api.timer = setTimeout(api.hide, qTipExtensions.notificationLifeTime);
    }
  }

  /**
   * Shows a dialog in the center of the page.
   * @param message Message that you want to show in the dialog.
   * @param title Title of the dialog, default: Dialog
   * @param modal Wether the dialog shall be modal or not, default: true
   */
  qTipExtensions.dialog = function(message, title, modal) {
    if (message === null || message === false || message === '')
      return;

    if (title === null || title === false || title === '')
      title = Drupal.t('Dialog');

    if (modal === null || modal === '')
      modal = true;

    if (!$(message).is('button') && $(message).find('button').length === 0) {
      if (typeof message === 'object')
        message = $(message).html();

      message += '<br /><br /><button>' + Drupal.t('Okay') + '</button>';
    }

    $(document.body).qtip({
      content: {
        text: message,
        title: title
      },
      position: {
        my: 'center',
        at: 'center',
        target: $(window)
      },
      show: {
        ready: true,
        modal: {
          on: modal,
          blur: !modal
        }
      },
      hide: false,
      style: {
        classes: Drupal.settings.qTipExtensions['dialog additional classes'] + ' qtip-extensions-dialog'
      },
      events: {
        render: function(event, api) {
          $.fn.qtip.defaults.events.render(event, api);
          $('button', api.elements.content).click(api.hide);
        },
        hide: function(event, api) {
          api.destroy();
        }
      }
    });
  };
})(jQuery);
